# fitology

It's hard to sign up to my favourite exercises. Let the script do it when I'm sleeping. 

### What you will need to start?
* This script is written in `Python2.7`
* `cron.yaml`
  * set `/pelvic_thursday` as your url and schedule it on every thursday at 00:01,
Europe/Warsaw timezone.
* `app.yaml`
  * Include following environment variables:
     * LOGIN_URL
     * REQ_URL
     * MEMBER_ID
     * LOGIN
     * PASSWORD
   * Add `ssl` to libraries
