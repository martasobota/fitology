# !/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date, timedelta

from fitology.fitology_data import check_holiday, next_week_date

def test_check_holiday_returns_properly_when_date_is_not_polish_holiday():
    result = check_holiday(date(2019, 10, 28))
    assert bool(result) == False
    assert result is None

def test_check_holiday_returns_properly_when_date_is_holiday():
    new_year = check_holiday(date(2019, 1, 1))
    three_kings = check_holiday(date(2019, 1, 6))
    easter = date(2019, 4, 21)
    easter_sunday = check_holiday(easter)
    easter_monday = check_holiday(easter + timedelta(days=1))
    labour_day = check_holiday(date(2019, 5, 1))
    may_3rd_constitution_day = check_holiday(date(2019, 5, 3))
    green_week = check_holiday(easter + timedelta(days=49))
    corpus_christi = check_holiday(easter + timedelta(days=60))
    assumption_of_mary = check_holiday(date(2019, 8, 15))
    all_saints_day = check_holiday(date(2019, 11, 1))
    independence_day = check_holiday(date(2019, 11, 11))
    christmas = check_holiday(date(2019, 12, 25))
    christmas_second_day = check_holiday(date(2019, 12, 26))
    assert new_year == 'Nowy Rok'
    assert three_kings == 'Święto Trzech Króli'
    assert easter_sunday == 'Niedziela Wielkanocna'
    assert easter_monday == 'Poniedziałek Wielkanocny'
    assert labour_day == 'Święto Państwowe'
    assert may_3rd_constitution_day == 'Święto Narodowe Trzeciego Maja'
    assert green_week == 'Zielone Świątki'
    assert corpus_christi == 'Dzień Bożego Ciała'
    assert assumption_of_mary == 'Wniebowzięcie Najświętszej Marii Panny'
    assert all_saints_day == 'Uroczystość Wszystkich świętych'
    assert independence_day == 'Narodowe Święto Niepodległości'
    assert christmas == 'Boże Narodzenie (pierwszy dzień)'
    assert christmas_second_day == 'Boże Narodzenie (drugi dzień)'

def test_next_week_day_counts_7_days_ahead():
    result = next_week_date(date(2019, 11, 1))
    assert result == '08-11-2019'

def test_next_week_day_returns_holiday_info():
    result = next_week_date(date(2019, 12, 18))
    assert result == None