# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""Fitology"""

import json
from datetime import date
import logging

import os
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from requests_toolbelt.adapters import appengine
from flask import Flask, Response

from fitology.fitology_data import (login, sign_up, next_week_date,
                                    current_calendar_url, html_soup, get_pelvic_id)
from fitology.secrets import get_secret

app = Flask(__name__)

production = os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/')
debugging = os.environ.get('DEBUG')
if production or debugging:
    appengine.monkeypatch()

login_url = get_secret('LOGIN_URL')
req_url = get_secret('REQ_URL')

login_marta = get_secret('LOGIN')
password_marta = get_secret('PASSWORD')
member_id_marta = get_secret('MEMBER_ID')

login_magda = get_secret('LOGIN_MAGDA')
password_magda = get_secret('PASSWORD_MAGDA')
member_id_magda = get_secret('MEMBER_ID_MAGDA')


@app.route('/marta_test')
def marta_test():
    next_7_days_date = next_week_date(date.today())
    if next_7_days_date:
        current_url = current_calendar_url(next_7_days_date)
        soup = html_soup(current_url)
        pelvic_id = get_pelvic_id(soup)
        session_marta = login(login_marta, password_marta, login_url)
        sign_up_info_marta = sign_up(
            session_marta, pelvic_id, member_id_marta, req_url)

        return_data = sign_up_info_marta
    else:
        return_data = "Next thursday is holiday time"
    logging.info(Response(response=json.dumps(return_data),
                          status=200, mimetype="application/json"))
    return Response(response=json.dumps(return_data).encode('utf-8'),
                    status=200, mimetype="application/json")


@app.route('/pelvic_thursday')
def pelvic_thursday():
    next_7_days_date = next_week_date(date.today())
    if next_7_days_date:
        current_url = current_calendar_url(next_7_days_date)
        soup = html_soup(current_url)
        id = get_pelvic_id(soup)
        session_marta = login(login_marta, password_marta, login_url)
        sign_up_info_marta = sign_up(session_marta, id, member_id_marta, req_url)

        session_magda = login(login_magda, password_magda, login_url)
        sign_up_info_magda = sign_up(session_magda, id, member_id_magda, req_url)

        return_data = (sign_up_info_marta, '\n',
                       sign_up_info_magda
                       )
    else:
        return_data = "Next thursday is holiday time"
    logging.info(Response(response=json.dumps(return_data),
                          status=200, mimetype="application/json"))
    return Response(response=json.dumps(return_data).encode('utf-8'),
                    status=200, mimetype="application/json")

@app.route('/pelvic/<name>')
def sign_in(name):
    next_7_days_date = next_week_date(date.today())
    if next_7_days_date:
        current_url = current_calendar_url(next_7_days_date)
        soup = html_soup(current_url)
        id_ = get_pelvic_id(soup)
        name_upper = name.upper()

        member_login = get_secret('LOGIN_{}'.format(name_upper))
        password = get_secret('PASSWORD_{}'.format(name_upper))
        member_id = get_secret('MEMBER_ID_{}'.format(name_upper))

        session = login(member_login, password, login_url)
        sign_up_info = sign_up(session, id_, member_id, req_url)
        if sign_up_info:
            print "Signup info: ", sign_up_info
            if 'Gratulujemy!' in sign_up_info.get('success_msg'):
                status=200
        else:
            status=500
        return_data = sign_up_info
    else:
        return_data = "Next thursday is holiday time"
    logging.info(Response(response=json.dumps(return_data),
                          status=status, mimetype="application/json"))
    return Response(response=json.dumps(return_data).encode('utf-8'),
                    status=status, mimetype="application/json")

if __name__ == '__main__':
    app.run()
