# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""Method receiving environ secrets"""

import os

def get_secret(key):
    """Returns os.environ value of passed key"""
    return os.environ.get(key)
