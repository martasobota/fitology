# !/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from datetime import timedelta

import holidays
import requests
from bs4 import BeautifulSoup as bs


def login(login, password, login_url):
    data = {'Login': login, 'Password': password,
            'Direct': '', 'SubmitCredentials': 'Zaloguj się'}
    session = requests.Session()
    session.post(login_url, data, verify=False)
    return session

def sign_up(session, exercises_id, member_id, req_url):
    fin_resp = session.post(
        req_url, {'id': exercises_id, 'memberID': member_id}, verify=False)
    sign_up_info = json.loads(fin_resp.content)
    is_logged = sign_up_info.get('IsLogged')
    success_msg = sign_up_info.get('SuccessMessage')
    payment = sign_up_info.get('PaymentRequired')
    payment_info = sign_up_info.get('PaymentInfo')
    error_msg = sign_up_info.get('ErrorMessage')
    return {'is_logged': is_logged, 'success_msg': success_msg,
            'payment': payment, 'payment_info': payment_info,
            'error_msg': error_msg}

def check_holiday(date):
    return holidays.PL().get(date)

def next_week_date(today):
    """today in datetime.date format"""
    next_week_date = (today + timedelta(days=7))
    if check_holiday(next_week_date):
        return None
    return next_week_date.strftime('%d-%m-%Y')

def current_calendar_url(today):
    return 'https://fitology-warszawa.cms.efitness.com.pl/kalendarz-zajec?day={}'.format(today)

def html_soup(current_url):
    page = requests.get(current_url)
    return bs(page.text, 'html.parser')

def get_pelvic_id(soup):
    events = soup.select(".event")
    for event in events:
        event_name = event.select('.event_name')
        if event_name[0].contents[0] == "Dno miednicy":
            id = event_name[0].contents[0].parent.find_parent('div')['meta:id']
            return id
